package com.hipages.hipagestwitter;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ListView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.AppSession;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterApiException;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.internal.TwitterApiConstants;
import com.twitter.sdk.android.core.models.Search;
import com.twitter.sdk.android.core.models.Tweet;

import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity {
    static final String TAG = "MainActivity";

    static final String TWITTER_KEY = "aspc3QcqYmv8840ce8SA7gaQ0";
    private static final String TWITTER_SECRET = "sKMrrsp1SnWEGrj9fB0qWxTt8OHQa1yPZpi31f2Gn3ZTevPl4L";
    AppSession guestAppSession;

    ListView list;
    LazyImageLoadAdapter adapter;
    Spinner spinner;
    Button searchButton;
    String keyword;
    EditText editText;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        list=(ListView)findViewById(R.id.list);

        spinner = (Spinner)findViewById(R.id.spinner);
        editText = (EditText)findViewById(R.id.editText);
        searchButton = (Button)findViewById(R.id.searchButton);

        searchButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(guestAppSession == null)
                {
                    showToast(R.string.info_notready);
                    loginTwitter();
                    return;
                }

                if(keywordValid(editText.getText().toString())) {
                    String prefix = "";
                    switch (spinner.getSelectedItemPosition()) {
                        case 0:
                            prefix = "@";
                        case 1:
                            prefix = "#";
                        case 2:
                        default:
                            prefix = "";
                    }
                    keyword = prefix + editText.getText().toString();
                    searchAction(guestAppSession,keyword);
                }
                else {
                    showToast(R.string.keyword_invalid_character);
                }

            }
        });

        loginTwitter();
    }

    public boolean keywordValid(String text)
    {
        if((text.indexOf(" ") >=0) || (text.indexOf("@")>=0) || (text.indexOf("#")>=0) )
        {
            Log.e(TAG,"keywordValid:" + text);
            return false;
        }
        return true;
    }

    public void loginTwitter()
    {
        TwitterAuthConfig authConfig =  new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        TwitterCore.getInstance().logInGuest(new Callback<AppSession>() {
            @Override
            public void success(Result<AppSession> result) {
                guestAppSession = result.data;
                showToast(R.string.login_succ);
            }

            @Override
            public void failure(TwitterException exception) {
                guestAppSession = null;
                // unable to get an AppSession with guest auth
                showToast(R.string.login_fail);
            }
        });
    }

    private void showToast(int resId)
    {
        Toast toast = Toast.makeText(MainActivity.this,getResources().getString(resId),Toast.LENGTH_LONG);
        toast.show();
    }

    public void searchAction(AppSession guestAppSession, String keyword)
    {
        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setTitle(R.string.progress_title);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        TwitterApiClient twitterApiClient =  TwitterCore.getInstance().getApiClient(guestAppSession);
        twitterApiClient.getSearchService().tweets(keyword, null, null, null, null, 50, null, null, null, true, new Callback<Search>() {
            @Override
            public void success(Result<Search> result) {
                // use result tweets
                Log.e(TAG, "success");
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                List<Pair<String, String>> resultList = new ArrayList<Pair<String, String>>();
                for (Tweet tweet : result.data.tweets) {
                    if (tweet != null && tweet.user != null) {
                        resultList.add(new Pair<String, String>(tweet.text, tweet.user.profileImageUrl));
                    }
                }

                // Create custom adapter for listview
                adapter = new LazyImageLoadAdapter(MainActivity.this, resultList);
                //Set adapter to listview
                list.setAdapter(adapter);
            }

            @Override
            public void failure(TwitterException exception) {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                final TwitterApiException apiException = (TwitterApiException) exception;
                final int errorCode = apiException.getErrorCode();
                if (errorCode == TwitterApiConstants.Errors.APP_AUTH_ERROR_CODE || errorCode == TwitterApiConstants.Errors.GUEST_AUTH_ERROR_CODE) {
                    // get new guestAppSession
                    // optionally retry
                    showToast(R.string.search_error_authfailed);
                } else {
                    showToast(R.string.search_error_unknown);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            showToast(R.string.settings_prompt);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy()
    {
        // Remove adapter refference from list
        list.setAdapter(null);
        super.onDestroy();
    }

    public OnClickListener listener=new OnClickListener(){
        @Override
        public void onClick(View arg0) {

            //Refresh cache directory downloaded images
            adapter.imageLoader.clearCache();
            adapter.notifyDataSetChanged();
        }
    };

    //add your click handling here for future use...
    public void onItemClick(int mPosition)
    {
    }
}
